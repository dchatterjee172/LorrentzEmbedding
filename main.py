import tensorflow as tf
import argparse
import json
from train import Model, train
from numpy import save
from pathlib import Path
from lorentz_embedding import lorentz_to_poincare

feature_description = {
    "word_index_left": tf.io.FixedLenFeature([], tf.int64),
    "word_index_right": tf.io.FixedLenFeature([], tf.int64),
}


def _parse_function(example_proto):
    return tf.io.parse_single_example(example_proto, feature_description)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("tfrecord_file", type=str)  # from create_tfrecord
    parser.add_argument("word_to_index_json", type=str)  # from create_tfrecord
    parser.add_argument("model_dir", type=Path)
    parser.add_argument("dim", type=int)
    parser.add_argument("--batch_size", type=int, default=32)
    parser.add_argument("--negative_sample_size", type=int, default=5)
    parser.add_argument("--epoch", type=int, default=100)
    parser.add_argument("--learning_rate", type=float, default=0.1)
    args = parser.parse_args()
    assert args.dim > 1
    with open(args.word_to_index_json) as f:
        word_to_index = json.load(f)
        index_to_word = {v: k for k, v in word_to_index.items()}
    with open(args.model_dir / "words_tensorboard", "w") as f:
        for i in range(2):  # left and right
            side = "LEFT" if i == 0 else "RIGHT"
            for index in sorted(index_to_word.keys()):
                f.write(f"{index_to_word[index]}_|_{side}\n")
    dataset = (
        tf.data.TFRecordDataset(args.tfrecord_file)
        .map(_parse_function)
        .shuffle(buffer_size=100000)
        .batch(args.batch_size)
    )
    model = Model(
        total_words=len(word_to_index),
        dim=args.dim,
        negative_sample_size=args.negative_sample_size,
    )
    summary_writer = tf.summary.create_file_writer(args.model_dir.as_posix())
    for emb in train(
        model=model,
        dataset=dataset,
        epoch=args.epoch,
        learning_rate=args.learning_rate,
        summary_writer=summary_writer,
        index_to_word=index_to_word,
    ):
        save(args.model_dir / "embeddings", lorentz_to_poincare(emb))
        model.save_weights((args.model_dir / "embedding.ckpt").as_posix())
