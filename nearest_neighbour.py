import argparse
import numpy as np
import difflib
import json


def distance(x, y):
    return np.arccosh(
        1
        + 2
        * (np.linalg.norm(x - y, ord=2, axis=-1) ** 2)
        / (
            (1 - np.linalg.norm(x, ord=2, axis=-1) ** 2)
            * (1 - np.linalg.norm(y, ord=2, axis=-1) ** 2)
        )
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("embedding", type=str)
    parser.add_argument("word_to_index_json", type=str)
    args = parser.parse_args()
    emb = np.load(args.embedding)
    index_to_word = dict()
    word_to_index = dict()
    with open(args.word_to_index_json) as f:
        word_to_index = json.load(f)
    index_to_word = {v: k for k, v in word_to_index.items()}
    print(len(index_to_word))
    word = input()
    if word in word_to_index:
        index = word_to_index[word]
        print(emb[index])
        dis = np.squeeze(
            distance(
                np.expand_dims(emb[index : (index + 1)], 1), np.expand_dims(emb, 0)
            ),
            0,
        )
        indices = np.argsort(dis, axis=-1)
        for i in indices[1:11]:
            print()
            print(dis[i], emb[i])
            try:
                print(index_to_word[i], word)
            except KeyError:
                print(word, index_to_word[i // 2])
    else:
        print("not present")
        words = difflib.get_close_matches(word, tuple(word_to_index.keys()))
        print(words)
        for w in words:
            print(w)
