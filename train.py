import tensorflow as tf
from lorentz_embedding import initialize_emb, l_distance, exp_map, proj
from tqdm import tqdm
from numpy import eye
import json


class Model(tf.keras.Model):
    def __init__(self, total_words, dim, negative_sample_size):
        super(Model, self).__init__()
        self.emb = tf.Variable(
            initial_value=initialize_emb(n=total_words * 2, d=dim),
            trainable=True,
            name="emb",
            shape=(total_words * 2, dim),
        )
        self.negative_sample_size = negative_sample_size
        self.total_words = total_words

    def call(self, word_index_left, word_index_right):
        emb_left = tf.gather(self.emb, word_index_left)
        batch_size = tf.shape(word_index_right)[0]
        negative_sample = tf.random.uniform(
            shape=(batch_size, self.negative_sample_size),
            dtype=tf.int64,
            minval=0,
            maxval=tf.cast(tf.shape(self.emb)[0] // 2, tf.int64),
        )
        sample = tf.concat((negative_sample, tf.expand_dims(word_index_right, 1)), 1)
        emb_right = tf.gather(self.emb, sample + self.total_words)
        emb_left = tf.expand_dims(emb_left, 1)
        dis = tf.squeeze(l_distance(emb_left, emb_right), 2)
        labels = tf.concat(
            (
                tf.zeros(
                    shape=(batch_size, self.negative_sample_size), dtype=tf.float32
                ),
                tf.ones(shape=(batch_size, 1), dtype=tf.float32),
            ),
            1,
        )
        labels = tf.fill([batch_size], self.negative_sample_size)
        loss = tf.nn.softmax_cross_entropy_with_logits(
            labels=tf.one_hot(
                labels, depth=self.negative_sample_size + 1, off_value=0.0
            ),
            logits=-dis,
        )
        return loss, -dis, labels, sample


def train(model, dataset, epoch, learning_rate, summary_writer, index_to_word):
    # The problem with this approach is, if a word has two embedding for left and right,
    # visualization becomes difficult. because "my name is" here to visualize properly,
    # we should have two vectors for "name" plotted in the same graph.
    # Plot it as "name_left" "name_right maybe"
    train_loss = tf.keras.metrics.Mean(name="train_loss")
    train_prec = tf.keras.metrics.Precision(name="train_precision")
    g = eye(tf.shape(model.emb).numpy()[1])
    g[0, 0] = -1
    g = tf.constant(g, dtype=tf.float32)

    @tf.function
    def train_step(word_index_left, word_index_right):
        with tf.GradientTape() as tape:
            loss, simi, labels, sample = model(word_index_left, word_index_right)
            loss = tf.reduce_mean(loss)
        gradient = tape.gradient(loss, model.emb, unconnected_gradients="zero")
        gradient = tf.clip_by_norm(gradient, 1)
        embs = tf.gather(model.emb, gradient.indices)
        transformed_grad = tf.matmul(gradient.values, g)
        tangent_space_proj = proj(embs, transformed_grad)
        exp_proj = exp_map(embs, -learning_rate * tangent_space_proj)
        x0 = tf.sqrt(
            1 + tf.reduce_sum(embs[..., 1:] * embs[..., 1:], axis=1, keepdims=True)
        )
        exp_proj = tf.concat((x0, exp_proj[..., 1:]), 1)
        new_emb = model.emb.scatter_nd_update(
            tf.expand_dims(gradient.indices, -1), exp_proj
        )
        pred = tf.argmax(simi, axis=-1)
        train_loss(loss)
        train_prec(
            tf.one_hot(labels, depth=model.negative_sample_size + 1),
            tf.one_hot(pred, depth=model.negative_sample_size + 1),
        )
        return new_emb, sample, pred

    step = 0
    for _ in tqdm(range(epoch)):
        train_loss.reset_states()
        train_prec.reset_states()
        for i, data in enumerate(tqdm(dataset)):
            emb, sample, pred = train_step(
                data["word_index_left"], data["word_index_right"]
            )
            if tf.random.uniform(shape=[], maxval=1) > 0.9:
                with summary_writer.as_default():
                    tf.summary.scalar("loss", train_loss.result(), step=step)
                    tf.summary.scalar("precision", train_prec.result(), step=step)
                    data_text = []
                    sample = sample.numpy()
                    pred = pred.numpy()
                    for i, (left, right) in enumerate(
                        zip(
                            data["word_index_left"].numpy(),
                            data["word_index_right"].numpy(),
                        )
                    ):
                        left = index_to_word[left]
                        right = index_to_word[right]
                        correct = f"{left} {right}"
                        pred_right = index_to_word[sample[i, pred[i]]]
                        pred_right = f"{left} {pred_right}"
                        data_text.append(dict(truth=correct, pred=pred_right))
                    tf.summary.text(
                        "text",
                        json.dumps(data_text, indent=2).replace("\n", "<br />"),
                        step=step,
                    )
            step += 1
        yield emb.numpy()
