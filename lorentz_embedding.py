import tensorflow as tf


def l_scalar_product(x, y):
    return -x[..., :1] * y[..., :1] + tf.reduce_sum(
        x[..., 1:] * y[..., 1:], axis=-1, keepdims=True
    )


def l_distance(x, y):
    product = -l_scalar_product(x, y)
    return tf.acosh(
        tf.math.maximum(product, tf.ones_like(product) + tf.keras.backend.epsilon())
    )


def exp_map(x, v):
    prod = l_scalar_product(v, v)
    t_norm_v = tf.sqrt(tf.maximum(prod, tf.zeros_like(prod)))
    return (
        tf.cosh(t_norm_v) * x
        + (tf.sinh(t_norm_v) / (t_norm_v + tf.keras.backend.epsilon())) * v
    )


def proj(x, u):
    return u + l_scalar_product(x, u) * x


def lorentz_to_poincare(x):
    return x[..., 1:] / (x[..., :1] + 1)


def initialize_emb(n, d):
    emb = tf.random.uniform(shape=(n, d - 1), minval=-0.001, maxval=0.001)
    x0 = tf.sqrt(1 + tf.reduce_sum(emb * emb, axis=1, keepdims=True))
    return tf.concat((x0, emb), axis=1)
