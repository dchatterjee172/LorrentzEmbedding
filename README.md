The dataset is from [here](https://www.ngrams.info/download_coca.asp)


Predicting the right word given the left word. For each left word, the correct right word and 20 randomly sampled word are given. The word vectors were 2 dimentional on poincare dim.


![loss_prec](scrnshot/im.png)


```
[
{
"truth": "be grand",
"pred": "be amused"
},
{
"truth": "British journalist",
"pred": "British journalist"
},
{
"truth": "because they",
"pred": "because they"
},
{
"truth": "and prenatal",
"pred": "and adventure"
},
{
"truth": "brief prayer",
"pred": "brief process"
},
{
"truth": "as Han",
"pred": "as surgery"
},
{
"truth": "Astros were",
"pred": "Astros were"
},
{
"truth": "been illegal",
"pred": "been accomplished"
},
{
"truth": "at Yale",
"pred": "at natural"
},
{
"truth": "beautiful a",
"pred": "beautiful a"
},
{
"truth": "both held",
"pred": "both held"
},
{
"truth": "bitter toward",
"pred": "bitter toward"
},
{
"truth": "and Christianity",
"pred": "and Christianity"
},
{
"truth": "and i",
"pred": "and addresses"
},
{
"truth": "and Fairfax",
"pred": "and n't"
},
{
"truth": "a clearinghouse",
"pred": "a pizza"
},
{
"truth": "as compiled",
"pred": "as compiled"
},
{
"truth": "be mutually",
"pred": "be notorious"
},
{
"truth": "area very",
"pred": "area very"
},
{
"truth": "all religion",
"pred": "all getting"
},
{
"truth": "Another wave",
"pred": "Another wave"
},
{
"truth": "battlegrounds of",
"pred": "battlegrounds of"
},
{
"truth": "as Christ",
"pred": "as months"
},
{
"truth": "Brendan Fraser",
"pred": "Brendan sensible"
},
{
"truth": "breaking new",
"pred": "breaking new"
},
{
"truth": "be hopelessly",
"pred": "be bound"
},
{
"truth": "an unacknowledged",
"pred": "an researched"
},
{
"truth": "balance while",
"pred": "balance while"
},
{
"truth": "be quoted",
"pred": "be quoted"
},
{
"truth": "between certain",
"pred": "between conservatives"
},
{
"truth": "and diagnoses",
"pred": "and fear"
},
{
"truth": "and brilliance",
"pred": "and essays"
},
{
"truth": "A viewer",
"pred": "A families"
},
{
"truth": "began shortly",
"pred": "began shortly"
},
{
"truth": "back against",
"pred": "back against"
},
{
"truth": "and Phillip",
"pred": "and Project"
}
```
