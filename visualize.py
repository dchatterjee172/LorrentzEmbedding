import altair as alt
from numpy import load
import argparse
import json

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("embedding", type=str)
    parser.add_argument("word_to_index_json", type=str)
    args = parser.parse_args()
    emb = load(args.embedding)
    assert emb.shape[-1] == 2
    with open(args.word_to_index_json) as f:
        word_to_index = json.load(f)
    data = alt.Data(
        values=[
            dict(
                x=float(emb[index + offset, 0]),
                y=float(emb[index + offset, 1]),
                word=word,
                position="left" if offset == 0 else "right",
            )
            for word, index in word_to_index.items()
            for offset in range(2)
        ]
    )
    alt.Chart(data).mark_circle().encode(
        x="x:Q",
        y="y:Q",
        tooltip=("word:O", "position:O"),
        color=alt.Color(
            "position:O",
            scale=alt.Scale(domain=("left", "right"), range=("red", "blue")),
        ),
    ).interactive().serve()
