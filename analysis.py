import csv
import argparse
from collections import Counter

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("twograms_file", type=str)
    args = parser.parse_args()
    all_words = Counter()
    with open(args.twograms_file, newline="", encoding="ISO-8859-1") as f:
        reader = csv.reader(f, delimiter="\t")
        for row in reader:
            all_words.update({row[1], row[2]})
    print(all_words.most_common(100), len(all_words))
