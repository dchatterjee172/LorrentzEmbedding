import tensorflow as tf
import csv
import argparse
from tqdm import tqdm
import json
from pathlib import Path


def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def serialize_example(word_index_left, word_index_right):
    feature = {
        "word_index_left": _int64_feature(word_index_left),
        "word_index_right": _int64_feature(word_index_right),
    }

    example_proto = tf.train.Example(features=tf.train.Features(feature=feature))
    return example_proto.SerializeToString()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "twograms_file", type=str
    )  # https://www.ngrams.info/download_coca.asp
    parser.add_argument("output_dir", type=Path)
    args = parser.parse_args()
    args.output_dir.mkdir(exist_ok=True)
    word_to_index = dict()
    with tf.io.TFRecordWriter((args.output_dir / "data.tfrecord").as_posix()) as writer:
        with open(args.twograms_file, newline="", encoding="ISO-8859-1") as f:
            reader = csv.reader(f, delimiter="\t")
            for row in tqdm(reader):
                if row[1] not in word_to_index:
                    word_to_index[row[1]] = len(word_to_index)
                if row[2] not in word_to_index:
                    word_to_index[row[2]] = len(word_to_index)
                word_index_left = word_to_index[row[1]]
                word_index_right = word_to_index[row[2]]
                example = serialize_example(word_index_left, word_index_right)
                writer.write(example)
    with open(args.output_dir / "word_to_index.json", "w") as f:
        json.dump(word_to_index, f, indent=2)
